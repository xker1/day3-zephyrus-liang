O:

    1. Learn how to conduct CodeReview, understand the role and benefits of CodeReview, and the rules for conducting CodeReview are:

        (1) Learning by Challenge and Sharing
        (2) Set time box per repo
        (3) Summary and Record the good point and question
        (4) Focus on the code and commit

    2. Learn how to use the Java Stream API, such as stream(), filter(), map(), etc., understand the concept of streams, and consolidate through practice.

    3. Reviewing the OOP programming paradigm, re-understanding the three major characteristics of object-oriented programming: encapsulation, inheritance, and polymorphism.

R: The difficulty of today’s class has increased compared to the previous two days, and I feel that my learning state is improving.

I: I have a certain understanding of Stream API and OOP, but when I actually code, I may find that my proficiency is not enough. I believe there are still many areas worth exploring in these two aspects.

D:I will try my best to use the Stream API in future operations on array classes to replace the commonly used for loop operations and improve my coding efficiency.

