package ooss.step7;

import ooss.Klass;
import ooss.Student;
import ooss.Teacher;
import ooss.observer.StudentObserver;
import ooss.observer.TeacherObserver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
public class ObserverTest {
    private ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @BeforeEach
    public void setup() {
        System.setOut(new PrintStream(outContent));
    }

    @Test
    public void should_print_message_when_assign_leader_given_teacher_is_teaching_the_class() {
        Klass klass = new Klass(2);
        Teacher jerry = new Teacher(1, "Jerry", 21);
        jerry.assignTo(klass);
        new TeacherObserver(klass, jerry);
        Student tom = new Student(1, "Tom", 18);
        tom.join(klass);

        klass.assignLeader(tom);

        assertThat(systemOut(), containsString("I am Jerry, teacher of Class 2. I know Tom become Leader."));
    }

    @Test
    public void should_print_message_when_assign_leader_given_another_student_is_in_the_class() {
        Klass klass = new Klass(2);
        Student eisen = new Student(1, "Eisen", 99);
        Student hugh = new Student(3, "Hugh", 99);
        eisen.join(klass);
        hugh.join(klass);
        new StudentObserver(klass, eisen);
        new StudentObserver(klass, hugh);
        Student Jason = new Student(3, "Jason", 99);
        Jason.join(klass);

        klass.assignLeader(Jason);

        assertThat(systemOut(), containsString("I am Eisen, student of Class 2. I know Jason become Leader."));
        assertThat(systemOut(), containsString("I am Hugh, student of Class 2. I know Jason become Leader."));
    }

    private String systemOut() {
        return outContent.toString();
    }
}
