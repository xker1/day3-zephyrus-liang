package ooss;

import ooss.observer.Observer;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Klass {
    private int number;
    private Student leader;
    private Teacher attachedTeacher;
    private Student attachedStudent;
    private final List<Observer> observers = new ArrayList<Observer>();

    public Klass(int number) {
        this.number = number;
    }

    public Klass(int number, Student leader) {
        this(number);
        this.leader = leader;
    }

    public Klass(int number, Student leader, Teacher attachedTeacher) {
        this(number, leader);
        this.attachedTeacher = attachedTeacher;
    }

    public Klass(int number, Student leader, Teacher attachedTeacher, Student attachedStudent) {
        this(number, leader, attachedTeacher);
        this.attachedStudent = attachedStudent;
    }

    public void attach(Observer observer) {
        observers.add(observer);
    }

    public void notifyAllObservers(){
        for (Observer observer : observers) {
            observer.update();
        }
    }

    public void assignLeader(Student student) {
        if (student.getKlass() != this)
            System.out.println("It is not one of us.");
        this.leader = student;
        notifyAllObservers();
    }
    public boolean isLeader(Student student) {
        if (this.leader == null)
            return false;
        return this.leader.equals(student);
    }

    public Teacher getAttachedTeacher() {
        return attachedTeacher;
    }

    public void setAttachedTeacher(Teacher attachedTeacher) {
        this.attachedTeacher = attachedTeacher;
    }

    public Student getAttachedStudent() {
        return attachedStudent;
    }

    public void setAttachedStudent(Student attachedStudent) {
        this.attachedStudent = attachedStudent;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public Student getLeader() {
        return leader;
    }

    public void setLeader(Student leader) {
        this.leader = leader;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Klass klass = (Klass) o;
        return number == klass.number;
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }
}
