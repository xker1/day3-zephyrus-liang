package ooss.observer;

import ooss.Klass;
import ooss.Person;

public abstract class Observer {
    protected Klass klass;
    public abstract void update();
}
