package ooss.observer;

import ooss.Klass;
import ooss.Student;

public class StudentObserver extends Observer{
    private final Student student;
    public StudentObserver(Klass klass, Student student) {
        this.klass = klass;
        this.student = student;
        this.klass.attach(this);
    }

    @Override
    public void update() {
        String assignIntroduceOnStudent= "I am " + student.getName() + ", student of Class " + klass.getNumber()
                + ". I know " + klass.getLeader().getName() + " become Leader.";
        System.out.println(assignIntroduceOnStudent);
    }
}
