package ooss.observer;

import ooss.Klass;
import ooss.Teacher;

public class TeacherObserver extends Observer {
    private final Teacher teacher;

    public TeacherObserver(Klass klass, Teacher teacher) {
        this.klass = klass;
        this.teacher = teacher;
        this.klass.attach(this);
    }

    @Override
    public void update() {
        String assignIntroduceOnTeacher= "I am " + teacher.getName() + ", teacher of Class " + klass.getNumber()
                + ". I know " + klass.getLeader().getName() + " become Leader.";
        System.out.println(assignIntroduceOnTeacher);
    }
}
