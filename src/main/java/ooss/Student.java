package ooss;

public class Student extends Person {

    private Klass klass;

    public Student(int id, String name, int age) {
        super(id, name, age);
    }

    public Student(int id, String name, int age, Klass klass) {
        super(id, name, age);
        this.klass = klass;
    }

    @Override
    public String introduce() {
        String introduce = super.introduce();
        introduce += " I am a student. I am in class " + this.klass.getNumber() + ".";
        if (this.klass.getLeader() == this)
            introduce += " I am the leader of class " + this.klass.getNumber() + ".";
        return introduce;
    }

    public void join(Klass klass){
        setKlass(klass);
    }

    public boolean isIn(Klass klass){
        if (this.klass == null)
            return false;
        else return this.klass.equals(klass);
    }

    public Klass getKlass() {
        return klass;
    }

    public void setKlass(Klass klass) {
        this.klass = klass;
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
